Source: phipack
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/phipack
Vcs-Git: https://salsa.debian.org/med-team/phipack.git
Homepage: https://www.maths.otago.ac.nz/~dbryant/software/phimanual.pdf
Rules-Requires-Root: no

Package: phipack
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: PHI test and other tests of recombination
 The PhiPack software package implements a few tests for recombination
 and can produce refined incompatibility matrices as well. Specifically,
 PHIPack implements the 'Pairwise Homoplasy Index', Maximum Chi2 and the
 'Neighbour Similarity Score'. The program Phi can be run to produce a
 p-value of recombination within a data set and the program profile can
 be run to determine regions exhibiting strongest evidence mosaicism.
